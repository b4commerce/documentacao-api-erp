#Documentação API B4C

## Descrição
Documentação da API Plataforma B4Commerce 

## Introdução

A API permite que sistemas possam consultar, incluir, alterar e remover informações na Loja Virtual. 


##Autenticação

A API utiliza o padrão “HTTP Basic Authentication”, que consiste em enviar o cabeçalho Authorization com um valor no formato Basic chave:token, onde chave:valor deve ser codificado em base64.

A chave e o token de acesso para a Api estão disponíveis no Gerenciador da Loja.

Importante: O cabeçalho de autenticação deve ser informado em todas as requisições enviadas para a API. 


##Endpoint

* [Produtos](api/produtos.md)
* [Categorias](api/categoria.md)
* [Pedidos](api/pedidos.md)
* [Clientes](api/clientes.md)
* [Marca](api/marca.md)
* [Precos](api/precos.md)


#Respostas

** A Api utiliza o HTTP status code para a resposta de toda requisição.

| Código | Descricao |
|------|-----------|
| 200 |  Sucesso |
| 201 | Criado |
| 400 | Requisição inválida |
| 401 | Credenciais  inválidas |
| 403 | Permissão negada ao recurso |
| 404 | A URL solicitada ou o recurso não existe |
| 405 | Método não permitido para o recurso |
| 409 | O recurso que está sendo criado já existe |
| 500 | Erro desconhecido. Por favor informe para o email suporte@b4commerce.com.br sobre o problema. |