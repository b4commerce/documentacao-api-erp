[Documentação - HOME](../README.md)

#clientes - ENDPOINT

## GET clientes/
**Listagem de clientes**

###Query parameters

| Parâmetro  | Default | Descrição                      |
|------------|---------|--------------------------------|
| includes   | null    | Inclusão de tabelas auxiliares |
| page       | 1       | Página da paginação            |
| offset     | 10      | Número de clientes por página  |

###Output

```json
{
	"clientes": [{
		"id": 1,
		"nome": "Fulano de Tal",
		"cpf_cnpj": "123456789-10",
		"rg_ie": "123456",
		"tipo": "1",
		"sexo": "1",
		"email": "fulano@fulano.com",
		"fone": "4199999999",
		"celular": "959999999",
		"dataNascimento": "01/01/1980",
		"clienteMarketPlace": false,
		"responsavel": "",
		"dataCadastro": "15/02/2018",
		"receberEmails": "true",
		"keyMundipagg": "0b7d3d4e-4e90-4db7-8491-6eee73c8bd9c",
		"tokenEmail": "859691",
		"ultimoAcesso": "18/02/2018",
		"creditos": 125.50,
		/*includes*/
		clientesEnderecos
		pedidos
		historicoCreditos
		acessos
		
	}]
}
```
| nome | descricao | tipo | tamanho | obrigatorio |
|------|-----------|------|---------|-------------|
| id | id do cliente | int |  11 | sim |
| nome | nome ou razão social do cliente | string |  100 | sim |
| cpf_cnpj |cpf/cnpj do cliente | string |  20 | sim |
| rg_ie | rg ou ie do cliente | string | 20 | sim |
| tipo | 1 Fisica ou 2 Juridica | int |  1 | sim |
| sexo | 1 Masculino 2 Feminino | int | 1 | sim |
| email | email do cliente  | string |  120| sim |
| fone | telefone | string | 16 | sim |
| celular | telefone celular | string | 16 | não |
| dataNascimento | data de nascimento | date | - | não |
| clienteMarketPlace | informe se o cadastro do cliente é de marketplace | boolean | - | não |
| dataCadastro | Data do cadastro | date | - | sim |
| receberEmails | Cliente autorizou o envio de promoções por email | boolean | - | sim |
| keyMundipagg | Chave de acesso vinculada ao mundipagg | string | 50 | não |
| tokenEmail | Token de verificação da conta | string | 6 | não |
| enderecosCliente | Listagem de endereços vinculadas a conta do cliente | array() | - | sim |  
| pedidosCliente | Listagem de pedidos efetuados pelo cliente | array() | - | sim |   
| creditos | Saldo em créditos do cliente | float | 11,2 | sim |   
| historicoCreditos | Histórico de utilização de créditos | array() | - | sim |  
| acessos | Histórico de acessos do cliente | array() | - | sim |  
  

##Tabelas auxiliares

* [clientesEmderecos](clientesEmderecos.md)
* [pedidos](pedidos.md)
* [historicoCreditos](historicoCreditos.md)
* [acessos](acessos.md)