[Documentação - HOME](../README.md)

#precos - ENDPOINT


###Query parameters

| API |  Descrição |
|------------|---------|
| GET GET /preco/?dataInicio={dataInicio}&dataFim={dataFim} |  Retorna todos os preço atualizados no período |
| GET /preco/{codigo} | Retorna o preço do produto  informado |                                                informado |
| PUT/preco/{codigo} | Remove o preço da listagem |

 
 

#GET /precos/?dataInicio={dataInicio}&dataFim={dataFim}
**Retorna todos os  preços atualizados no período
**Requisição
**Parâmetros da Url

| Nome | Descrição | Tipo  | Obrigatóriol |
|------------|---------|--------------------------------|
| dataInicio |   | String | Sim |
| dataFim |   | String | Sim |
 
###Resposta

| nome | descricao | tipo | 
|------|-----------|------|
| codigo_sku | codigo do produto | string |
| data_atualizacao | data da atualização do preço | date |
| ultimo_preco | preço anterior do produto | float | 
| preco |  preço do produto | float |


###Output

```json
    [
        {
            "codigo_sku":"1",
            "data_atualizacao":"2018-07-05 13:09:54",
            "ultimo_preco":"0.00",
            "preco":"145.00",
           
            ]
        },
        {
        
            "codigo_sku":"8228",
            "data_atualizacao":"2018-06-07 11:39:55",
            "ultimo_preco":"413.83",
            "preco":"376.21",
        
        }

    ]
```

#GET /precos/{codigo}
**Retorna o  preço atual do produto
**Requisição
**Parâmetros da Url

| Nome | Descrição | Tipo  | Obrigatóriol |
|------------|---------|--------------------------------|
| codigo | sku(codigo do produto no ERP)  | String | Sim |
 
###Resposta

| nome | descricao | tipo | 
|------|-----------|------|
| codigo_sku | codigo do produto | string |
| data_atualizacao | data da atualização do preço | date |
| ultimo_preco | preço anterior do produto | float | 
| preco |  preço do produto | float |


###Output

```json
    [
        {
            "codigo_sku":"1",
            "data_atualizacao":"2018-07-05 13:09:54",
            "ultimo_preco":"0.00",
            "preco":"145.00",
          
        }
    ]
```



#PUT /preco/{codigo}
**Requisição
**Parâmetros da Url

| Nome | Descrição | Tipo  | Obrigatório|
|------------|---------|--------------------------------|
| codigo | sku(codigo do produto no ERP) | String | Sim |

 
*Body


| nome | descricao | tipo | Obrigatório |
|------|-----------|------|---------|
| tipo | valor =2 para confirmar a atualização | int | sim |

###Exemplo

```json
 {
    "tipo":2
 }

```
**Resposta
httpResponseCode

