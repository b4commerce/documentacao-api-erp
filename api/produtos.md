[Documentação - HOME](../README.md)

#produtos - ENDPOINT


###Query parameters

| API |  Descrição |
|------------|---------|
| GET /produtos/?={Offest}&Limit={Limit} |  Retorna todos os produtos |
| GET /produtos/?={codigo} | Retorna informações do produto  informado |                                                informado |
| POST /produtos | Cadastra um novo produto |
| PATCH /produtos/{codigo} |  Atualiza as informações do produto |
| PUT/produtos/{codigo} |  Retorna todos os produtos |
| DELETE/produtos/{codigo} | Atualiza o status do produto para inativo |
 
 

#GET /produtos/?={Offest}&Limit={Limit}
**Retorna todos os produtos
**Requisição
**Parâmetros da Url

| Nome | Descrição | Tipo  | Inf Adicional |
|------------|---------|--------------------------------|
| Offset | Ignora os primeiros { Offset } itens | Integer | |
| Limit | Define a quantidade de registros | Integer | 200 registros |

**Requisição
**Parâmetros da Url
#GET /produtos/{codigo}
**Retorna informações sobre um produto cadastrado

| Nome | Descrição | Tipo  | Obrigatório|
|------------|---------|--------------------------------|
| codigo | código do produto no ERP | String | Sim |

###Resposta

| nome | descricao | tipo | 
|------|-----------|------|
| nome | nome do produto | string | 
| titulo_seo| titulo do produto | string | 
| subtitulo | subtitulo do produto | string | 
| descricao | descrição do produto | string |
| meta_descricao | meta description do produto | string |
| codigo_grupo | id do grupo | string |
| codigo_categoria | codigo da categoria | string |
| codigo_subcategoria | codigo da subcategoria | string |
| codigo_marca | codigo da marca o  | string |
| palavras_chave | palavras chave para o produto | string |
| ean | código de barras do produto | string |
| ncm | código de barras do produto | string |
| modelo | modelo do produto | string |
| fora_de_linha | produto fora de linha | int |   
| estoque | estoque total do produto| int |   
| estoque_minimo | estoque minimo para o produto|  int |  
| estoques_filiais | lista o estoque das filiais | lista de * [estoques](api/estoques_filiais.md) | 
| dimensoes | lista peso as dimensões do produto para o envio |  lista de * [dimensoes](api/produtos_dimensoes.md) | 
| imagens | lista de imagens do produto| lista de * [imagens](api/produtos_imagens.md)|
| unidade_de_venda | unidade de venda para o produto | string |
| disponivel | disponibilidade do  produto | boolean |
| preco_custo | preço de custo do produto | float ) |
| custos_filiais | lista de custos do produto em cada fiial | lista de * [precos_custo](api/produtos_preco_custo.md) |
| preco | valor de venda do produto | float |
| prod_cod_fabricante | código do produto no fabricante | string |
| status_sistema | situação do produto no ERP  | int |
| prod_cod_fabricante | código do produto no fabricante | string |
| prod_cod_referencia | referencia do produto no fabricante | string |
| id_garantia | identificador do tipo de garantia do produto | int |
| cd_comprador | código(ERP) do comprador vinculado ao produto | int |
| ipi | percentual de IPI para o produto | float |
| CrossDocking | informa se o produto está no crossdocking | string |

###Output

```json
{"clientes" :[{ "codigo": "43893",
"nome": " Furadeira Gsb550-Re 550W",
"titulo_seo":"Furadeira de Impacto Reversível Variável Gsb550-Re 550W",
"subtitulo":"Potencia e precisão"
"codigo": "43893",
"descricao":" - Conforto e Produtividade: Formato compacto e
ergonônico facilita o trabalho e evita o cansaço mesmo durante o uso
prolongado
- Robusta: Resistente mandril de 13 mm feito em metal de alta qualidade
- Controle ideal: Interruptor eletrônico com velocidade variável.
- Botão-Trava: Excelentes para trabalhos contínuos.",
"meta_descricao":"Furadeira Bosch em Oferta. Confira!",
"cfop": "6108",
"ncm": "8467.11.10",
"cest": "806",
"cst": "5",
"codigo_categoria": "6",
"codigoMarca": "1",
"palavrasChave": "FURADEIRA,FURADEIRA ELÉTRICA",
"ncm":"1234"
"modelo": " Gsb550-Re",
"ean": " 7891009828657",
"fora_de_linha ": false,
"estoque": [{
"quantidade" :"1",
"filial" : "1",
}],
"estoqueMinimo": 2,
"dimensoes": {
"altura": 10,
"largura": 25,
"comprimento": 30,
"peso": 1000
},
"imagens": [
{
"imagem":
"http://url_da_imagem",
"ordem": 1
},
{
"imagem":
"http://url_da_imagem
"ordem": 2
}
],
"grupos": [
{
"nome": "Especificações Técnicas",
"atributos": [
{"nome": "Potência",
"valor": "550 watts"
},
{
"nome": "Rotação por minuto (RPM)",
"valor": "0-3.150"
}
]
}
],
"variacoes": [
{
"IdGrupoVariacao ": "1",
"NomeGrupoVariacao": "Voltagem",
"IdVariacao ": "100",
"NomeVariacao": "127 Volts"
}
],
"unidade_de_venda": "UN"
"disponivel": true,
"preco_custo":104.96,
"custos_filiais":[  
{
       "preco_custo" :105.00,
       "filial" : "1",
 },
 {
        "preco_custo" :109.00,
        "filial" : "3",
  }
  
 
 
 ],
"preco ": 180.00
"prod_cod_fabricante":"1",
"status_sistema":"2",
"prod_referencia":"5",
"garantia":12,
"cross_docking":false}
}]
}
```

#POST /produtos
**Cadastra informações de um novo produto
**Requisição
**Parâmetros da Url
Nenhum
*Body


| nome | descricao | tipo | Obrigatório |
|------|-----------|------|---------|
| codigo | código sku do produto | string | sim |
| nome | nome do produto | string | sim |
| titulo_seo| titulo do produto | string | 
| subtitulo | subtitulo do produto | string | 
| descricao | descrição do produto | string |
| meta_descricao | meta description do produto | string |
| codigo_grupo | id do grupo | string |
| codigo_categoria | codigo da categoria | string |
| codigo_subcategoria | codigo da subcategoria | string |
| codigo_marca | codigo da marca o  | string |
| palavras_chave | palavras chave para o produto | string |
| ean | código de barras do produto | string |
| ncm | código de barras do produto | string |
| modelo | modelo do produto | string |
| fora_de_linha | produto fora de linha | int |   
| estoque | estoque total do produto| int |   
| estoque_minimo | estoque minimo para o produto|  int |  
| estoques_filiais | lista o estoque das filiais | lista de * [estoques](api/estoques_filiais.md) | 
| dimensoes | lista peso as dimensões do produto para o envio |  lista de * [dimensoes](api/produtos_dimensoes.md) | 
| imagens | lista de imagens do produto| lista de * [imagens](api/produtos_imagens.md)|
| unidade_de_venda | unidade de venda para o produto | string |
| disponivel | disponibilidade do  produto | boolean |
| preco_custo | preço de custo do produto | float ) |
| custos_filiais | lista de custos do produto em cada fiial | lista de * [precos_custo](api/produtos_preco_custo.md) |
| preco | valor de venda do produto | float |
| prod_cod_fabricante | código do produto no fabricante | string |
| status_sistema | situação do produto no ERP  | int |
| prod_cod_fabricante | código do produto no fabricante | string |
| prod_cod_referencia | referencia do produto no fabricante | string |
| id_garantia | identificador do tipo de garantia do produto | int |
| cd_comprador | código(ERP) do comprador vinculado ao produto | int |
| ipi | percentual de IPI para o produto | float |
| CrossDocking | informa se o produto está no crossdocking | string |

###Exemplo

```json
{"clientes" :[{ "codigo": "43893",
"nome": " Furadeira Gsb550-Re 550W",
"titulo_seo":"Furadeira de Impacto Reversível Variável Gsb550-Re 550W",
"subtitulo":"Potencia e precisão"
"codigo": "43893",
"descricao":" - Conforto e Produtividade: Formato compacto e
ergonônico facilita o trabalho e evita o cansaço mesmo durante o uso
prolongado
- Robusta: Resistente mandril de 13 mm feito em metal de alta qualidade
- Controle ideal: Interruptor eletrônico com velocidade variável.
- Botão-Trava: Excelentes para trabalhos contínuos.",
"meta_descricao":"Furadeira Bosch em Oferta. Confira!",
"cfop": "6108",
"ncm": "8467.11.10",
"cest": "806",
"cst": "5",
"codigo_categoria": "6",
"codigoMarca": "1",
"palavrasChave": "FURADEIRA,FURADEIRA ELÉTRICA",
"ncm":"1234"
"modelo": " Gsb550-Re",
"ean": " 7891009828657",
"fora_de_linha ": false,
"estoque": [{
"quantidade" :"1",
"filial" : "1",
}],
"estoqueMinimo": 2,
"dimensoes": {
"altura": 10,
"largura": 25,
"comprimento": 30,
"peso": 1000
},
"imagens": [
{
"imagem":
"http://url_da_imagem",
"ordem": 1
},
{
"imagem":
"http://url_da_imagem
"ordem": 2
}
],
"grupos": [
{
"nome": "Especificações Técnicas",
"atributos": [
{"nome": "Potência",
"valor": "550 watts"
},
{
"nome": "Rotação por minuto (RPM)",
"valor": "0-3.150"
}
]
}
],
"variacoes": [
{
"IdGrupoVariacao ": "1",
"NomeGrupoVariacao": "Voltagem",
"IdVariacao ": "100",
"NomeVariacao": "127 Volts"
}
],
"unidade_de_venda": "UN"
"disponivel": true,
"preco_custo":104.96,
"custos_filiais":[  
{
       "preco_custo" :105.00,
       "filial" : "1",
 },
 {
        "preco_custo" :109.00,
        "filial" : "3",
  }
  
 
 
 ],
"preco ": 180.00
"prod_cod_fabricante":"1",
"status_sistema":"2",
"prod_referencia":"5",
"garantia":12,
"cross_docking":false}
}]
}
```
**Resposta
httpResponseCode

#PUT /produtos/{codigo}
**Requisição
**Parâmetros da Url

| Nome | Descrição | Tipo  | Obrigatório|
|------------|---------|--------------------------------|
| codigo | código do produto no ERP | String | Sim |

 
*Body


| nome | descricao | tipo | Obrigatório |
|------|-----------|------|---------|
| codigo | código sku do produto | string | sim |
| nome | nome do produto | string | sim |
| titulo_seo| titulo do produto | string | 
| subtitulo | subtitulo do produto | string | 
| descricao | descrição do produto | string |
| meta_descricao | meta description do produto | string |
| codigo_grupo | id do grupo | string |
| codigo_categoria | codigo da categoria | string |
| codigo_subcategoria | codigo da subcategoria | string |
| codigo_marca | codigo da marca o  | string |
| palavras_chave | palavras chave para o produto | string |
| ean | código de barras do produto | string |
| ncm | código de barras do produto | string |
| modelo | modelo do produto | string |
| fora_de_linha | produto fora de linha | int |   
| estoque | estoque total do produto| int |   
| estoque_minimo | estoque minimo para o produto|  int |  
| estoques_filiais | lista o estoque das filiais | lista de * [estoques](api/estoques_filiais.md) | 
| dimensoes | lista peso as dimensões do produto para o envio |  lista de * [dimensoes](api/produtos_dimensoes.md) | 
| imagens | lista de imagens do produto| lista de * [imagens](api/produtos_imagens.md)|
| unidade_de_venda | unidade de venda para o produto | string |
| disponivel | disponibilidade do  produto | boolean |
| preco_custo | preço de custo do produto | float ) |
| custos_filiais | lista de custos do produto em cada fiial | lista de * [precos_custo](api/produtos_preco_custo.md) |
| preco | valor de venda do produto | float |
| prod_cod_fabricante | código do produto no fabricante | string |
| status_sistema | situação do produto no ERP  | int |
| prod_cod_fabricante | código do produto no fabricante | string |
| prod_cod_referencia | referencia do produto no fabricante | string |
| id_garantia | identificador do tipo de garantia do produto | int |
| cd_comprador | código(ERP) do comprador vinculado ao produto | int |
| ipi | percentual de IPI para o produto | float |
| CrossDocking | informa se o produto está no crossdocking | string |

###Exemplo

```json
{"clientes" :[{ 
"nome": " Furadeira Gsb550-Re 550W",
"titulo_seo":"Furadeira de Impacto Reversível Variável Gsb550-Re 550W",
"subtitulo":"Potencia e precisão"
"codigo": "43893",
"descricao":" - Conforto e Produtividade: Formato compacto e
ergonônico facilita o trabalho e evita o cansaço mesmo durante o uso
prolongado
- Robusta: Resistente mandril de 13 mm feito em metal de alta qualidade
- Controle ideal: Interruptor eletrônico com velocidade variável.
- Botão-Trava: Excelentes para trabalhos contínuos.",
"meta_descricao":"Furadeira Bosch em Oferta. Confira!",
"cfop": "6108",
"ncm": "8467.11.10",
"cest": "806",
"cst": "5",
"codigo_categoria": "6",
"codigoMarca": "1",
"palavrasChave": "FURADEIRA,FURADEIRA ELÉTRICA",
"ncm":"1234"
"modelo": " Gsb550-Re",
"ean": " 7891009828657",
"fora_de_linha ": false,
"estoque": [{
"quantidade" :"1",
"filial" : "1",
}],
"estoqueMinimo": 2,
"dimensoes": {
"altura": 10,
"largura": 25,
"comprimento": 30,
"peso": 1000
},
"imagens": [
{
"imagem":
"http://url_da_imagem",
"ordem": 1
},
{
"imagem":
"http://url_da_imagem
"ordem": 2
}
],
"grupos": [
{
"nome": "Especificações Técnicas",
"atributos": [
{"nome": "Potência",
"valor": "550 watts"
},
{
"nome": "Rotação por minuto (RPM)",
"valor": "0-3.150"
}
]
}
],
"variacoes": [
{
"IdGrupoVariacao ": "1",
"NomeGrupoVariacao": "Voltagem",
"IdVariacao ": "100",
"NomeVariacao": "127 Volts"
}
],
"unidade_de_venda": "UN"
"disponivel": true,
"preco_custo":104.96,
"custos_filiais":[  
{
       "preco_custo" :105.00,
       "filial" : "1",
 },
 {
        "preco_custo" :109.00,
        "filial" : "3",
  }
  
 
 
 ],
"preco ": 180.00
"prod_cod_fabricante":"1",
"status_sistema":"2",
"prod_referencia":"5",
"garantia":12,
"cross_docking":false}
}]
}
```
**Resposta
httpResponseCode

